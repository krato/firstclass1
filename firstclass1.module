<?php

class FirstClass1 {

    public function __get($name) {
        $method_name = 'get' . ucfirst($name);
        if (!method_exists($this, $method_name)) {
            trigger_error('Method doesn\'t exists: ' . $method_name, E_USER_ERROR);
            return ;
            //throw new FirstClass1Exception1('Method doesn\'t exists: ' . $method_name);
        }
        return $this->$method_name();
    }

    public function __set($name, $value) {
        $method_name = 'set' . ucfirst($name);
        $this->$method_name($value);
    }

}

trait FirstTrait1 {

    public function __get($name) {
        if ($name == '') {
            trigger_error('Property name can\'t be empty.', E_USER_WARNING);
        }
        $methods = get_class_methods($this);
        $method_name = 'get' . ucfirst($name);
        if (in_array($method_name, $methods)) {
            return $this->$method_name();
        } else {
            return $this->$name;
        }
    }

    public function __set($name, $value) {
        if ($name == '') {
            trigger_error('Property name can\'t be empty.', E_USER_WARNING);
        }
        $methods = get_class_methods($this);
        $method_name = 'set' . ucfirst($name);
        if (in_array($method_name, $methods)) {
            $this->$method_name($value);
        } else {
            $this->$name = $value;
        }
    }

}

/**
 * @property-read array $raw Raw Drupal array encapsulated by this class
 */
class FirstClass1Raw extends FirstClass1 implements ArrayAccess {

    public $raw;

    public function __construct(&$raw = array()) {
        if (!is_null($raw)) {
            if (is_object($raw)) {
                assert(get_class($raw) == get_class($this));
                $this->raw = $raw->raw;
            } else {
                assert(is_array($raw));
                $this->raw = &$raw;
            }
        }
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->raw[] = $value;
        } else {
            $this->raw[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->raw[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->raw[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->raw[$offset]) ? $this->raw[$offset] : null;
    }

}

class RenderMarkup1 extends FirstClass1Raw {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'item';
    }

    public function set($name, $value) {
        $this->raw[$name] = $value;
    }

    public function get($name) {
        return $this->raw[$name];
    }

    public function is_set($name) {
        return isset($this->raw[$name]);
    }

    public function addWrappingTag($htmlTagName) {
        assert($htmlTagName[0] != '<');
        $prefix = isset($this->raw['#prefix']) ? $this->raw['#prefix'] : '';
        $suffix = isset($this->raw['#suffix']) ? $this->raw['#suffix'] : '';
        $this->raw['#prefix'] = $prefix . '<' . $htmlTagName . '>';
        $this->raw['#suffix'] = '</' . $htmlTagName . '>' . $suffix;
        return $this;
    }

    public function setTitle($title) {
        $this->raw['#title'] = $title;
        return $this;
    }

    public function setMarkup($markup) {
        $this->raw['#markup'] = $markup;
        return $this;
    }

    public function setTheme($themeCallback) {
        $this->raw['#theme'] = $themeCallback;
        return $this;
    }
    
    public function setWeight($weight) {
        assert(is_numeric($weight));
        $this->raw['#weight'] = $weight;
        return $this;
    }

    public function addClass($className) {
        if (!isset($this->raw['#attributes'])) {
            $this->raw['#attributes'] = [];
        }
        if (!isset($this->raw['#attributes']['class'])) {
            $this->raw['#attributes']['class'] = [];
        }
        $this->raw['#attributes']['class'][] = $className;
        return $this;
    }

    public function addAttributes($attributes) {
        if (!isset($this->raw['#attributes'])) {
            $this->raw['#attributes'] = [];
        }
        $this->raw['#attributes'] += $attributes;
        return $this;
    }

}

class RenderItem1 extends RenderMarkup1 {

    public function setTitle($title) {
        $this->raw['#title'] = $title;
        return $this;
    }

    public function __construct() {
        $this->raw['#type'] = 'item';
        parent::__construct();
    }

}

class TableCell1 {

    public $header;
    public $class;
    public $value;

    public function __construct($header, $class, $data) {
        $this->header = $header;
        $this->class = $class;
        $this->data = $data;
    }

}

class MenuItem1 extends FirstClass1Raw {

    function setTitle($title) {
        $this->raw['title'] = $title;
        return $this;
    }

    function setDescription($description) {
        $this->raw['description'] = $description;
        return $this;
    }

    /**
     * <b>"type"</b>: A bitmask of flags describing properties of the menu item. Many shortcut bitmasks are provided as constants in menu.inc:
     * <ul>
     * <li>MENU_NORMAL_ITEM: Normal menu items show up in the menu tree and can be moved/hidden by the administrator.</li>
     * <li>MENU_CALLBACK: Callbacks simply register a path so that the correct information is generated when the path is accessed.</li>
     * <li>MENU_SUGGESTED_ITEM: Modules may "suggest" menu items that the administrator may enable.</li>
     * <li>MENU_LOCAL_ACTION: Local actions are menu items that describe actions on the parent item such as adding a new user or block, and are rendered in the action-links list in your theme.</li>
     * <li>MENU_LOCAL_TASK: Local tasks are menu items that describe different displays of data, and are generally rendered as tabs.</li>
     * <li>MENU_DEFAULT_LOCAL_TASK: Every set of local tasks should provide one "default" task, which should display the same page as the parent item.</li>
     * </ul>
     * @param type $type
     * @return \MenuItem1
     */
    function setType($type) {
        $this->raw['type'] = $type;
        return $this;
    }

    function setPage($callback = NULL, $arguments = NULL) {
        if ($callback !== NULL) {
            $this->raw['page callback'] = $callback;
        }
        if ($arguments !== NULL) {
            assert(is_array($arguments));
            $this->raw['page arguments'] = $arguments;
        }
        return $this;
    }
    
    /**
     * This do: $this->setPage('drupal_get_form', [$callback]);
     * @param string $callback
     * @return \MenuItem1
     */
    function setForm($callback) {
        $this->setPage('drupal_get_form', [$callback]);
        return $this;
    }

    function setAccess($callback = NULL, $arguments = NULL) {
        if ($callback !== NULL) {
            $this->raw['access callback'] = $callback;
        }
        if ($arguments !== NULL) {
            if (!is_array($arguments)) {
                $arguments = (array) $arguments;
            }
            $this->raw['access arguments'] = $arguments;
        }
        return $this;
    }

    function setWeight($weight) {
        assert(is_int($weight));
        $this->raw['weight'] = $weight;
        return $this;
    }

}

/**
 * @property-write String $title
 * @property-write misc $defaultValue
 * @property-write misc $value
 * @property-write String $description
 * @property-write boolean $disabled
 */
class FormControl1 extends RenderItem1 {

    function setDefaultValue($value) {
        $this->raw['#default_value'] = $value;
        return $this;
    }

    function setValue($value) {
        $this->raw['#value'] = $value;
        return $this;
    }

    function setDescription($description) {
        $this->raw['#description'] = $description;
        return $this;
    }

    function setDisabled($disabled = TRUE) {
        $this->raw['#disabled'] = (bool) $disabled;
        return $this;
    }

    function setRequired($required = TRUE) {
        $this->raw['#required'] = (bool) $required;
        return $this;
    }

}

/**
 * @property-write String[] $options Associative array of option_id =&lt; option_label
 */
class FormSelect1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'select';
    }

    /**
     * @param array $options Associative map option_id => option_title
     */
    function setOptions($options) {
        $this->raw['#options'] = $options;
        return $this;
    }

}

/**
 * @property-write int $length Max allowed length
 */
class FormTextfield1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'textfield';
    }

    function setMaxlength($length) {
        $this->raw['#maxlength'] = $length;
        return $this;
    }

    function setSize($size) {
        $this->raw['#size'] = $size;
        return $this;
    }

}

/**
 * @property-write int $cols Columns
 * @property-write int $rows Rows
 */
class FormTextarea1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'textarea';
    }

    public function setCols($cols) {
        $this->raw['#cols'] = $cols;
        return $this;
    }

    public function setRows($rows) {
        $this->raw['#rows'] = $rows;
        return $this;
    }

}

class FormTextFormat1 extends FormTextarea1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'text_format';
    }

    public function setFormat($format) {
        $this->raw['#format'] = $format;
        return $this;
    }

}

class FormSubmit1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'submit';
    }

    public function addSubmitCallback($callback) {
        $this->raw['#submit'][] = $callback;
        return $this;
    }

}

class Config1 {

    protected static $defaultDefaultValue = '';

    protected function defaults() {
        return [];
    }

    public function __get($name) {
        $defaults = $this->defaults();
        if (isset($defaults[$name])) {
            return variable_get($name, $defaults[$name]);
        } else {
            return variable_get($name, self::$defaultDefaultValue);
        }
    }

    public function __set($name, $value) {
        variable_set($name, $value);
    }

}

/**
 * @property-write bool $collapsible
 * @property-write bool $collapsed
 */
class FormFieldset1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'fieldset';
        $this->raw['#collapsible'] = TRUE;
        $this->raw['#collapsed'] = FALSE;
    }

    function setCollapsible($collapsible) {
        $this->raw['#collapsible'] = $collapsible;
        return $this;
    }

    function setCollapsed($collapsed) {
        $this->raw['#collapsed'] = $collapsed;
        return $this;
    }

}

class Menu1 extends FirstClass1Raw {
    
}

class Form1 extends FirstClass1Raw {
    
}

/**
 * @property-write array $header Header elements
 * @property-write array $rows Rows
 * @property-write string $caption
 */
class Table1 extends FirstClass1Raw {

    public function __construct() {
        parent::__construct();
        $this->raw['#theme'] = 'table';
    }

    function setHeader($header) {
        $this->raw['#header'] = $header;
        return $this;
    }

    function setRows($rows) {
        $this->raw['#rows'] = $rows;
        if (!empty($rows)) {
            $cols = reset($rows);
            foreach ($cols as $col) {
                if (isset($col['title'])) {
                    $this->raw['#header'][] = [
                        'data' => $col['title'],
                    ];
                }
            }
        }
        return $this;
    }

    function setCaption($text) {
        $this->raw['#caption'] = $text;
        return $this;
    }

}

class TableHeaderCell1 extends FirstClass1Raw {

    function setData($data) {
        $this->raw['data'] = $data;
        return $this;
    }

    function setClass($class) {
        $this->raw['class'] = $class;
        return $this;
    }

    function setField($field) {
        $this->raw['field'] = $field;
        return $this;
    }

    function setColspan($colspan) {
        $this->raw['colspan'] = $colspan;
        return $this;
    }

}

class TableRowCell1 extends FirstClass1Raw {

    function setTitle($title) {
        $this->raw['title'] = $title;
        return $this;
    }

    function setData($data) {
        $this->raw['data'] = $data;
        return $this;
    }

    function setColspan($count) {
        $this->raw['colspan'] = $count;
        return $this;
    }

    function setRowspan($count) {
        $this->raw['rowspan'] = $count;
        return $this;
    }

    function setClass($class) {
        $this->raw['class'] = $class;
        return $this;
    }

    function set($name, $value) {
        $this->raw[$name] = $value;
        return $this;
    }

}

class Link1 extends RenderMarkup1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'link';
    }

    function setTitle($title) {
        $this->raw['#title'] = $title;
        return $this;
    }

    function setHref($href) {
        $this->raw['#href'] = $href;
        return $this;
    }

    function setOptions($options) {
        $this->raw['#options'] = $options;
        return $this;
    }

    /**
     * Sets return URL, e.g. to drupal_get_destination(), must by in array
     */
    function setDestinationURL($destionation) {
        $this->addParameters($destionation);
        return $this;
    }

    function addParameters($parameters) {
        assert(is_array($parameters));
        if (!isset($this->raw['#options'])) {
            $this->raw['#options'] = [];
        }
        if (!isset($this->raw['#options']['query'])) {
            $this->raw['#options']['query'] = [];
        }
        $this->raw['#options']['query'] += $parameters;
        return $this;
    }

}

abstract class SchemaField1 extends FirstClass1Raw {

    /**
     * @param string $size Size of the field, see: https://drupal.org/node/159605
     * @return \SchemaField1
     */
    function setSize($size) {
        $this->raw['size'] = $size;
        return $this;
    }

    function setNotNull($notNull) {
        $this->raw['not null'] = $notNull;
        return $this;
    }

    function setDefault($defaultValue) {
        $this->raw['default'] = $defaultValue;
        return $this;
    }

    function setDescription($description) {
        $this->raw['description'] = $description;
        return $this;
    }

}

class SchemaSerial1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'serial';
        $this->raw['unsigned'] = TRUE;
        $this->raw['not null'] = TRUE;
    }

}

class SchemaInt1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'int';
    }

    public function setUnsigned($unsigned = TRUE) {
        $this->raw['unsigned'] = (bool) $unsigned;
        return $this;
    }

}

class SchemaFloat1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'float';
    }

}

class SchemaNumeric1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'numeric';
    }

}

class SchemaVarchar1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'varchar';
    }

}

class SchemaChar1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'char';
    }

}

class SchemaText1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'text';
    }
    
    /**
     * MySQL: tinytext, 256 B
     */
    public function setSizeToTiny() {
       return $this->setSize('tiny'); 
    }

    /**
     * MySQL: tinytext, 256 B
     */
    public function setSizeToSmall() {
       return $this->setSize('small'); 
    }

    /**
     * MySQL: mediumtext, 16 MB
     */
    public function setSizeToMedium() {
       return $this->setSize('medium'); 
    }

    /**
     * MySQL: longtext, 4 GB
     */
    public function setSizeToBig() {
       return $this->setSize('big'); 
    }

    /**
     * MySQL: text, 16 KB
     */
    public function setSizeToNormal() {
       return $this->setSize('normal'); 
    }

}

class SchemaBlob1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw['type'] = 'blob';
    }

}

/**
 * Inspired by: https://drupal.org/node/159605
 */
class SchemaDateTime1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw += [
            'type' => 'datetime',
            'mysql_type' => 'DATETIME',
            'pgsql_type' => 'timestamp without time zone',
            'sqlite_type' => 'VARCHAR',
            'sqlsrv_type' => 'smalldatetime',
            'sortable' => TRUE,
            'views' => TRUE,
        ];
    }

}

/**
 * Inspired by: https://drupal.org/node/159605
 */
class SchemaDate1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw += [
            'type' => 'datetime',
            'mysql_type' => 'DATE',
            'sqlite_type' => 'VARCHAR',
            'sortable' => TRUE,
            'views' => TRUE,
        ];
    }

}

/**
 * Inspired by: https://drupal.org/node/159605
 */
class SchemaTime1 extends SchemaField1 {

    public function __construct() {
        parent::__construct();
        $this->raw += [
            'type' => 'text',
            'size' => 'tiny',
            'mysql_type' => 'TIME',
            'sqlite_type' => 'VARCHAR',
            'sortable' => TRUE,
            'views' => TRUE,
        ];
    }

}

class SchemaForeignKey1 extends FirstClass1Raw {

    function setTable($tableName) {
        assert(is_string($tableName));
        $this->raw['table'] = $tableName;
        return $this;
    }

    function setColumns($columns) {
        assert(is_array($columns));
        $this->raw['columns'] = $columns;
        return $this;
    }

}

class FormPasswordConfirm1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'password_confirm';
    }

    function setSize($size) {
        $this->raw['#size'] = $size;
        return $this;
    }

}

class FormDate1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'date';
    }

}

class FormFile1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'file';
    }

}

class FormCheckbox1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'checkbox';
    }

}

class FormRadios1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'radios';
    }

    /**
     * @param array $options Associative map option_id => option_title
     */
    function setOptions($options) {
        $this->raw['#options'] = $options;
        return $this;
    }

}

define('ANY', -1);

/**
 * @property int $id ID, default is ANY.
 */
class Conditions1 extends FirstClass1Raw {

    public function __construct($id = ANY) {
        $this->id = $id;
    }

    public function setId($id) {
        assert(is_string($id) || is_int($id));
        assert($id >= 1 || $id == ANY);
        if ($id >= 1) {
            $this->raw['id'] = $id;
        } else {
            unset($this->raw['id']);
        }
        return $this;
    }

    public function getId() {
        return $this->raw['id'];
    }

}

class FormValue1 extends FormControl1 {

    public function __construct() {
        parent::__construct();
        $this->raw['#type'] = 'value';
    }

}

/**
 * @property-read string $type
 * @property-read int $nid
 * @property-read int $vid
 * @property-read int $uid
 * @property-read DateTime $created
 * @property-read DateTime $changed
 * @property bool $comment
 * @property bool $promote
 * @property bool $sticky
 * @property bool $status NODE_PUBLISHED or NODE_NOT_PUBLISHED
 * @property-read string $language language code or LANGUAGE_NONE
 * @property int $tnid
 * @property bool $translate
 * @property string $log
 */
abstract class Node1Node extends FirstClass1 {

    public $raw;

    protected function setLog($text) {
        assert(is_string($text));
        $this->raw->log = $text;
        return $this;
    }

    protected function getLog() {
        return $this->raw->log;
    }

    protected function getTnid() {
        return $this->raw->tnid;
    }

    protected function getTranslate() {
        return $this->raw->translate;
    }

    protected function getLanguage() {
        return $this->raw->language;
    }

    protected function getStatus() {
        return $this->raw->status;
    }

    public function setStatus($enabled) {
        $this->raw->status = $enabled ? 1 : 0;
        return $this;
    }

    public function setComment($enabled) {
        $this->raw->comment = $enabled ? 1 : 0;
        return $this;
    }

    public function setPromote($enabled) {
        $this->raw->promote = $enabled ? 1 : 0;
        return $this;
    }

    public function setSticky($enabled) {
        $this->raw->sticky = $enabled ? 1 : 0;
        return $this;
    }

    protected function getComment() {
        return $this->raw->comment;
    }

    protected function getPromote() {
        return $this->raw->promote;
    }

    protected function getSticky() {
        return $this->raw->sticky;
    }

    protected function getCreatedRaw() {
        return $this->raw->created;
    }

    protected function getCreated() {
        return $this->raw->created;
    }

    protected function getChangedRaw() {
        return $this->raw->changed;
    }

    protected function getChanged() {
        return $this->raw->changed;
    }

    public function __construct($raw = NULL) {
        global $user;
        if ($raw) {
            assert(is_object($raw));
            assert($raw->type == static::getNodeType());
            $this->raw = $raw;
        } else {
            $this->raw = new stdClass;
            $this->raw->uid = $user->uid;
            $this->raw->name = (isset($user->name) ? $user->name : '');
            $this->raw->type = static::getNodeType();
            $this->raw->title = '';
            $this->raw->language = LANGUAGE_NONE;
            node_object_prepare($this->raw);
        }
    }

    protected static function getNodeType() {
        trigger_error('Error: Function getNodeType is not overriden.');
    }

    protected function extractFieldElement($fieldName) {
        assert(is_string($fieldName));
        if (property_exists($this->raw, $fieldName)) {
            foreach ($this->raw->$fieldName as $langcode => $fieldDataSet) {
                foreach ($fieldDataSet as $fieldData) {
                    return $fieldData;
                }
            }
        }
        return NULL;
    }

    public function save() {
        node_save($this->raw);
    }

    public function delete() {
        node_delete($this->raw->nid);
    }

    public function editForm($form, &$form_state) {
        module_load_include('inc', 'node', 'node.pages');
        return node_form($form, $form_state, $this->raw);
    }

    protected function getUid() {
        return $this->raw->uid;
    }

    protected function getTitle() {
        return $this->raw->title;
    }

    protected function getNid() {
        return $this->raw->nid;
    }

    protected function getVid() {
        return $this->raw->vid;
    }

    protected function getType() {
        return static::getNodeType();
    }

}

abstract class Node1Query {

    /**
     * @var EntityFieldQuery
     */
    public $entityFieldQuery;

    protected static function getNodeType() {
        trigger_error('Node type is not specified. The function getNodeType must be overriden.');
    }

    function __construct($nid = NULL, $vid = NULL) {
        $this->entityFieldQuery = (new EntityFieldQuery())
                ->entityCondition('entity_type', 'node')
                ->entityCondition('bundle', static::getNodeType())
                ->propertyCondition('status', 1)
                ->propertyOrderBy('created', 'DESC'); // from newest by default
        if ($nid) {
            $this->entityFieldQuery->propertyCondition('nid', $nid);
        }
        if ($vid) {
            $this->entityFieldQuery->propertyCondition('vid', $vid);
        }
    }

    /**
     * @param mixed $status EntityFieldQuery::RETURN_ALL or NODE_PUBLISHED or NODE_NOT_PUBLISHED
     * @return \Node2Query
     */
    public function setStatus($status) {
        assert(
                $status == EntityFieldQuery::RETURN_ALL ||
                $status == NODE_PUBLISHED ||
                $status == NODE_NOT_PUBLISHED
        );
        foreach ($this->entityFieldQuery->propertyConditions as $key => $propertyCondition) {
            if ($propertyCondition['column'] == 'status') {
                unset($this->entityFieldQuery->propertyConditions[$key]);
            }
        }
        if ($status == NODE_PUBLISHED || $status == NODE_NOT_PUBLISHED) {
            $this->entityFieldQuery->propertyCondition('status', $status);
        }
        return $this;
    }

    public function all() {
        $result1 = $this->entityFieldQuery->execute();
        if (empty($result1['node'])) {
            return [];
        }
        $nids = array_keys($result1['node']);
        $result2 = node_load_multiple($nids);
        return !empty($result2) ? $result2 : [];
    }

    public function get() {
        $all = $this->all();
        if (empty($all)) {
            trigger_error(t('Node not found.'));
        }
        return reset($all);
    }

}
